## Running Docker dengan Gitlab CI/CD
Helwo, ini adalah repository untuk membantu Anda yang ingin belajar tentang bagaimana membuat docker image & container menggunakan CI/CD.
Ini steps yang bisa Anda lakukan berikut.

## Install Docker Engine & Gitlab Runner

1. Pasang Docker sesuai dengan instruksi & OS yang dipakai (This case is using Debian) disini https://docs.docker.com/engine/install/debian/#install-using-the-repository
2. Install Gitlab Runner dengan link berikut https://docs.gitlab.com/runner/install/linux-manually.html#using-debrpm-package & Ikuti konfigurasi berikut.
```
root@debian-docker-test:~# gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=14495 revision=8ec04662 version=16.3.0
Running in system-mode.                            
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.com
Enter the registration token:
glrt-IsiTokenDariGitlab        <--- Token ini bisa kalian generate dari menu Project/Settings/CI/CD/Runners/New Project Runner
                                    Masukkan Tags sesuai keinginan kalian & Create Runner, nantinya akan ada token yang harus 
                                    Anda paste ke CLI
Verifying runner... is valid                        runner=uSs8k4zCS
Enter a name for the runner. This is stored only in the local config.toml file:
[debian-docker-test]: docker-shell-runner         <----- Isikan nama runner sesuai keinginan kalian
Enter an executor: virtualbox, docker-autoscaler, docker+machine, instance, kubernetes, custom, docker, docker-windows, parallels, shell, ssh:
shell       <---- Kita akan gunakan shell karena kita ingin semua repo ini masuk ke folder runner & dieksekusi langsung di sana

Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
 
Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml"
```
3. Allow gitlab-runner agar dapat mengeksekusi docker dengan command berikut.
```
root@debian-docker-test:~# usermod -aG docker gitlab-runner
```

## Meracik Dockerfile
1. Buat file "Dockerfile" dan masukkan sesuai berikut.
```
FROM nginx:1.25-alpine    # Untuk mengambil image yang terbaru dari NginX

ADD index.html /usr/share/nginx/html   # Mengambil file dari dalam repo & ditransfer ke docker

EXPOSE 80    # Anda tau makna "Terbuka" kan ?

CMD ["nginx", "-g", "daemon off;"]    # Menyuruh CLI untuk mengeksekusi command ini ke Container
```
2. Save dengan nama "Dockerfile".


## Mengatur Gitlab CI/CD
1. Buat file ".gitlab-ci.yml" untuk membuat "automation" agar dapat melakukan build docker image & bisa menjadikan sebagai container.
2. Ikuti steps-by-steps berikut.
```
   1. Pertama, Buat terlebih dahulu stages untuk operation nya.

stages:
  - build    # Diperuntukkan untuk build image.
  - run      # Setelah build, langsung dibuatkan container nya & langsung dijalankan

   2. Kedua, buatlah before_script yang dimana kita gunakan untuk menambahkan SSH ke Runner nya.

before_script:      # Script ini akan digunakan untuk menambahkan SSH Private agar dapat diakses dari server Gitlab nya.
  - eval $(ssh-agent -s)
  - echo "$SSH_Keys" | tr -d '\r' | ssh-add -
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
  - ssh-keyscan -H $IP_Addr >> ~/.ssh/known_hosts
  - chmod 644 ~/.ssh/known_hosts

   3. Ketiga, Kita buat setiap stages dengan script sesuai perintah nya, Build & Run

Build:              # Stage ini untuk build Image docker
  stage: build
  script:
    - docker build --no-cache -t webbase .
  
Activate:           # Stage ini untuk running docker image nya yang sudah dibuild sebelumnya
  stage: run
  before_script:
    - docker stop nginx-saia && docker rm nginx-saia
  script:
    - docker run -d -p 80:80 --name nginx-saia webbase:latest
  needs:
    - job: Build
```
3. Tambahkan variable dan masukkan ke dalam menu Settings/CI/CD/Variables & Click "Expand" dan "Add Variable"
```
| Keys     | Value    | Attributes | Environments |
| -------- | -------- | --------   | ------------ |
| IP_Addr  | ****     | Expanded   | All          | <-------- Untuk IP Address Runner
| SSH_Keys | ****     | Expanded   | All          | <-------- SSH Key yang kamu punya di server tersebut
```

## Test & Check it
Yang bisa Anda lakukan distep ini adalah cek IP Address kalian. Jika keluar website "AHOOOOYYYY"
Then, it works.

## End of Line
Jika masih ada kendala, silahkan email ke dhiky.cancerio@gmail.com, Jelaskan error yang dihadapi & code yang Anda buat sebelumnya.
Sekian dari saya, Dhiky sign out.